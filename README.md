# Isla de Calor Urbana

Este repositorio contiene un modelo para el cómputo espacialmente explícito de islas de calor en zonas urbanas. Está basado en [este artículo](https://doi.org/10.2134/jeq2015.01.0056). Se puede explorar una versión simplificada en [esta hoja de cálculo](modelo_mitigacion_balance.xlsx).

Con base en esa hoja de cálculo hemos desarrollado una interfaz programática en Python. Combina un modelo de [flujo de calor latente](uhi/__init__.py) con un [modelo vegetal](uhi/planta.py). Este repo incluye un [notebook para explorar el modelo](single_day_data_trial.ipynb).

Nuestro objetivo es dearrollar software interactivo que permita el análisis de diferentes trayectorias del sistema. O sea, se podrán elegir diferentes temporadas del año o establecer temperaturas, velocidades del viento y temperaturas, junto con capas planificadas de reforestación urbana, para explorar distintos escenarios de isla de calor urbana.

## Flujo de datos

Hemos descargado datos de estaciones metereológicas de fuentes públicas. Desarrollamos software para [transformar](raw2shape.R) los datos al formato **shp** compatible con QGIS. Insumos para el modelo son **temperatura, humedad relativa y velocidad del viento**.

Dentro de QGIS interpolamos los datos de las estaciones a las otras celdas del raster. Así obtenemos rasters de temperatura, humedad relativa y velocidad del viento.

```
python3 data_interpolation_headless.py 2021-01-02
```


Desarrollamos [este script](heat_flux.py) que toma la data de cada celda para ejecutar el [modelo](uhi) y de ahí generar rasters con los valores de *output* del modelo.

```
python3 heat_flux.py 2021-01-02
```

![data flow](dataflow.png)


### Origen de datos De la Ciudad de México

Se pueden obtener en esta liga: <http://www.aire.cdmx.gob.mx/default.php?opc=%27aKBhnmI=%27&opcion=Zw==>

No incluimos los datos de CDMX en el repositorio. Para descargarlos se sugiere clonar este repositorio, luego situarse en el directorio data y correr el comando **make**, que tiene recetas para descargar los datos.

```
cd data
make
```

### Datos de Toluca

Se pueden obtener buscando "meteorología" aquí: <http://datos.edomex.gob.mx/>

Por tratarse de cinco archivos, los hemos incluido en este repositorio, en el [directorio de datos](data/edomex).

## Flujo de calor latente

En la siguiente gráfica mostramos una representación visual de una de las salidas del modelo. Los puntos negros son las ubicaciones de las estaciones, las gradientes de color representan los valores de flujo de calor latente, computados con el modelo para cada celda del raster.

![datos interpolados](preview.png)

Esta visualización es un avance de los productos que esperamos obtener.



