import imp
import sys
import os
import argparse
from qgis.core import *
from qgis.analysis import QgsRasterCalculatorEntry, QgsRasterCalculator


from PyQt5.QtWidgets import QApplication

app = QApplication([])
QgsApplication.initQgis()


# Prepare processing framework 
sys.path.append('/usr/share/qgis/python/plugins') 
from processing.core.Processing import Processing
Processing.initialize()
import processing
import pathlib

parser = argparse.ArgumentParser(
  description="""interpolate temp, rh and wsp between stations""")

parser.add_argument('date',
                  help='date to construct path to input shapes YYYY-mm-dd')

args = parser.parse_args()

fecha = args.date

year, month, day = fecha.split("-")


for hora in range(6,19):
  rh_input = "data/shapes/%s/%s/%s/%s-%s_rh.shp" % (year, month, day, fecha, hora)
  rh_output = "data/shapes/%s/%s/%s/%s-%s_rh_utm.shp" % (year, month, day, fecha, hora)
  rh_tif_prefix = "data/tifs/%s/%s/%s/%s-%s_rh" % (year, month, day, fecha, hora)

  tmp_input = "data/shapes/%s/%s/%s/%s-%s_tmp.shp" % (year, month, day, fecha, hora)
  tmp_output = "data/shapes/%s/%s/%s/%s-%s_tmp_utm.shp" % (year, month, day, fecha, hora)
  tmp_tif_prefix = "data/tifs/%s/%s/%s/%s-%s_tmp" % (year, month, day, fecha, hora)

  wsp_input = "data/shapes/%s/%s/%s/%s-%s_wsp.shp" % (year, month, day, fecha, hora)
  wsp_output = "data/shapes/%s/%s/%s/%s-%s_wsp_utm.shp" % (year, month, day, fecha, hora)
  wsp_tif_prefix = "data/tifs/%s/%s/%s/%s-%s_wsp" % (year, month, day, fecha, hora)

  mask_layer_path = "data/extent_islas.shp"

  pathlib.Path("data/tifs/%s/%s/%s/" % (year, month, day)).mkdir(parents=True, exist_ok=True)

  print('reproject tmp', tmp_input, tmp_output)
  processing.run("native:reprojectlayer", {'INPUT': tmp_input,
                                          'TARGET_CRS':QgsCoordinateReferenceSystem('EPSG:32614'),
                                          'OPERATION':'+proj=pipeline +step +proj=unitconvert +xy_in=deg +xy_out=rad +step +proj=utm +zone=14 +ellps=WGS84',
                                          'OUTPUT': tmp_output})

  print('reproject rh', rh_input, rh_output)
  processing.run("native:reprojectlayer", {'INPUT': rh_input,
                                          'TARGET_CRS':QgsCoordinateReferenceSystem('EPSG:32614'),
                                          'OPERATION':'+proj=pipeline +step +proj=unitconvert +xy_in=deg +xy_out=rad +step +proj=utm +zone=14 +ellps=WGS84',
                                          'OUTPUT': rh_output})

  print('reproject wsp', wsp_input, wsp_output)
  processing.run("native:reprojectlayer", {'INPUT': wsp_input,
                                          'TARGET_CRS':QgsCoordinateReferenceSystem('EPSG:32614'),
                                          'OPERATION':'+proj=pipeline +step +proj=unitconvert +xy_in=deg +xy_out=rad +step +proj=utm +zone=14 +ellps=WGS84',
                                          'OUTPUT': wsp_output})

  print('interpolate rh', rh_input, rh_output)
  processing.run("gdal:gridinversedistance", {'INPUT': rh_output,
                                              'Z_FIELD':'RH',
                                              'POWER':2.2,
                                              'SMOOTHING':0,
                                              'RADIUS_1':0,
                                              'RADIUS_2':0,
                                              'ANGLE':0,
                                              'MAX_POINTS':0,
                                              'MIN_POINTS':0,
                                              'NODATA':0,
                                              'OPTIONS':'',
                                              'EXTRA':'',
                                              'DATA_TYPE':5,
                                              'OUTPUT': rh_tif_prefix + '_pre.tif'})

  print('interpolate wsp', wsp_input, wsp_output)
  processing.run("gdal:gridinversedistance", {'INPUT': wsp_output,
                                              'Z_FIELD':'WSP',
                                              'POWER':2.2,
                                              'SMOOTHING':0,
                                              'RADIUS_1':0,
                                              'RADIUS_2':0,
                                              'ANGLE':0,
                                              'MAX_POINTS':0,
                                              'MIN_POINTS':0,
                                              'NODATA':0,
                                              'OPTIONS':'',
                                              'EXTRA':'',
                                              'DATA_TYPE':5,
                                              'OUTPUT': wsp_tif_prefix + '_pre.tif'})    

  print('interpolate tmp', tmp_input, tmp_output)
  processing.run("gdal:gridinversedistance", {'INPUT': tmp_output,
                                              'Z_FIELD':'TMP',
                                              'POWER':2.2,
                                              'SMOOTHING':0,
                                              'RADIUS_1':0,
                                              'RADIUS_2':0,
                                              'ANGLE':0,
                                              'MAX_POINTS':0,
                                              'MIN_POINTS':0,
                                              'NODATA':0,
                                              'OPTIONS':'',
                                              'EXTRA':'',
                                              'DATA_TYPE':5,
                                              'OUTPUT': tmp_tif_prefix + '_pre.tif'})    



  #raster calculator para clipear

  # o mejor con cliprasterbymasklayer?

  processing.run("gdal:cliprasterbymasklayer", {'INPUT': wsp_tif_prefix + '_pre.tif',
                                                'MASK': mask_layer_path,
                                                'ALPHA_BAND': False,
                                                'CROP_TO_CUTLINE': True,
                                                'SET_RESOLUTION': True,
                                                'X_RESOLUTION': 100.0,
                                                'Y_RESOLUTION': 100.0,
                                                'OUTPUT': wsp_tif_prefix + '.tif',
                                                })



  processing.run("gdal:cliprasterbymasklayer", {'INPUT': tmp_tif_prefix + '_pre.tif',
                                                'MASK': mask_layer_path,
                                                'ALPHA_BAND': False,
                                                'CROP_TO_CUTLINE': True,
                                                'SET_RESOLUTION': True,
                                                'X_RESOLUTION': 100.0,
                                                'Y_RESOLUTION': 100.0,
                                                'OUTPUT': tmp_tif_prefix + '.tif',
                                                })

  processing.run("gdal:cliprasterbymasklayer", {'INPUT': rh_tif_prefix + '_pre.tif',
                                                'MASK': mask_layer_path,
                                                'ALPHA_BAND': False,
                                                'CROP_TO_CUTLINE': True,
                                                'SET_RESOLUTION': True,
                                                'X_RESOLUTION': 100.0,
                                                'Y_RESOLUTION': 100.0,
                                                'OUTPUT': rh_tif_prefix + '.tif',
                                                })



