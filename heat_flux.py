import os
import argparse
import sys
from pathlib import Path
import numpy as np
import rasterio
#import geopandas as gp
#import pandas
import uhi
from uhi import planta as plnt
from uhi import secundarias as sec
from rasterio.warp import transform

parser = argparse.ArgumentParser(
  description="""calculate heat flux""")

parser.add_argument('date',
                  help='date to construct path to input shapes YYYY-mm-dd')

parser.add_argument('vpath',
                  help='path to vegetation raster')


args = parser.parse_args()

fecha = args.date
vpath = Path(args.vpath)

experiment = vpath.name.split('.')[0]

year, month, day = fecha.split("-")

def array_from_raster(file):
    r = rasterio.open(file)
    r_array = r.read()
    r.close()
    return r_array

def save_array(array, path):
    with rasterio.Env():
        profile = src.profile
        with rasterio.open(path, 'w', **profile) as dst:
            dst.write(array)



BASE_DIR = Path(__file__).resolve().parent

veg = array_from_raster(vpath)



data_dir = os.path.join(BASE_DIR, "data/tifs/%s/%s/%s/" % (year, month, day))

os.chdir(data_dir)

outdir = os.path.join(datadir, experiment)


for hora in range(7,19):
    src = rasterio.open('%s-%s_tmp.tif' % (fecha, hora))
    temp0_array = array_from_raster('%s-%s_tmp.tif' % (fecha, hora-1))
    temp1_array = array_from_raster('%s-%s_tmp.tif' % (fecha, hora))
    rh_array = array_from_raster("%s-%s_rh.tif" % (fecha, hora))
    wsp_array = array_from_raster('%s-%s_wsp.tif' % (fecha, hora))

    (kk, height, width) = rh_array.shape

    
    que_array = np.zeros((1,height,width))
    qev_array = np.zeros((1,height,width))
    qhu_array = np.zeros((1,height,width))
    qhv_array = np.zeros((1,height,width))
    tdpu_array = np.zeros((1,height,width))
    tdpv_array = np.zeros((1,height,width))
    qn_array = np.zeros((1,height,width))
    delta_qs_array = np.zeros((1,height,width))

    for x in range(width):
        for y in range(height):
            temp0 = temp0_array[0][y][x]
            temp0_abs = temp0 + 273.15
            temp1 = temp1_array[0][y][x]
            temp1_abs = temp1 + 273.15
            rh = rh_array[0][y][x]
            wsp = wsp_array[0][y][x]
            xs, ys = rasterio.transform.xy(src.transform, y, x)
            lon, lat = transform('EPSG:32614','EPSG:4326', [xs], [ys])
            lon = lon[0]
            lat = lat[0]
            estacion = uhi.Estacion(lon=lon, lat=lat)
            S = sec.S(temp1)
            que = estacion.QUE(S, hora, temp0_abs, temp1_abs)
            qhu = estacion.QHU(S, hora, temp0_abs, temp1_abs)
            tdpu = estacion.TD_PU(hora, S, temp0_abs, temp1_abs)
            QN = estacion.QN(hora, temp1_abs)
            
            p = plnt.Planta()
            if veg[0][y][x]:
                wsp = max(wsp, 0.001)   # que hacer cuando wsp es 0?
                temp1 = max(temp1, 0.001)   # que hacer cuando temp1 es 0?
                DPV = sec.DPV(sec.es(temp1), sec.e(sec.es(temp1), rh))
                Cp = sec.calor_especifico_aire()

                #print("qn",QN,"temp1",temp1,"DPV",DPV,"wsp",wsp)
                qe = p.QE(Cp, temp1, QN, DPV, wsp)
                qhv = estacion.QHV(hora, temp0_abs, temp1_abs, qe)
                tdpv = estacion.TD_PV(hora, temp0_abs, temp1_abs, qe)
            else:
                S = sec.S(temp1)
                qe = que
                qhv = qhu
                tdpv = tdpu
            
            que_array[0][y][x] = que
            qev_array[0][y][x] = qe
            qhu_array[0][y][x] = qhu
            qhv_array[0][y][x] = qhv
            tdpu_array[0][y][x] = tdpu
            tdpv_array[0][y][x] = tdpv
            qn_array[0][y][x] = QN
            delta_qs_array[0][y][x] = estacion.delta_QS(hora=hora, 
                                                        temp_abs0=temp0_abs,
                                                        temp_abs1=temp1_abs)

    
    save_array(que_array, f'{outdir}/que_{experiment}_{fecha}-{hora}.tif')
    save_array(qev_array, f'{outdir}/qev_{experiment}_{fecha}-{hora}.tif')
    save_array(qn_array, f'{outdir}/qn_{experiment}_{fecha}-{hora}.tif')
    save_array(delta_qs_array, f'{outdir}/delta_qs_{experiment}_{fecha}-{hora}.tif')
    save_array(qhu_array, f'{outdir}/qhu_{experiment}_{fecha}-{hora}.tif')
    save_array(qhv_array, f'{outdir}/qhv_{experiment}_{fecha}-{hora}.tif')
    save_array(tdpu_array, f'{outdir}/tdpu_{experiment}_{fecha}-{hora}.tif')
    save_array(tdpv_array, f'{outdir}/tdpv_{experiment}_{fecha}-{hora}.tif')


