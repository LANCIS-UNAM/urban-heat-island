import os
from pathlib import Path

# requires saga in qgis
processing.run("native:reprojectlayer", {'INPUT':'/home/fidel/codeberg/urban-heat-island/data/01_01_2021_06_00.shp',
                                        'TARGET_CRS':QgsCoordinateReferenceSystem('EPSG:32614'),
                                        'OPERATION':'+proj=pipeline +step +proj=unitconvert +xy_in=deg +xy_out=rad +step +proj=utm +zone=14 +ellps=WGS84',
                                        'OUTPUT':'/home/fidel/codeberg/urban-heat-island/data/01_01_2021_06_00_utm.shp'})


processing.run("native:reprojectlayer", {'INPUT':'/home/fidel/codeberg/urban-heat-island/data/01_01_2021_06_00_rh.shp',
                                        'TARGET_CRS':QgsCoordinateReferenceSystem('EPSG:32614'),
                                        'OPERATION':'+proj=pipeline +step +proj=unitconvert +xy_in=deg +xy_out=rad +step +proj=utm +zone=14 +ellps=WGS84',
                                        'OUTPUT':'/home/fidel/codeberg/urban-heat-island/data/01_01_2021_06_00_rh_utm.shp'})
                                        
processing.run("native:reprojectlayer", {'INPUT':'/home/fidel/codeberg/urban-heat-island/data/01_01_2021_06_00_wsp.shp',
                                        'TARGET_CRS':QgsCoordinateReferenceSystem('EPSG:32614'),
                                        'OPERATION':'+proj=pipeline +step +proj=unitconvert +xy_in=deg +xy_out=rad +step +proj=utm +zone=14 +ellps=WGS84',
                                        'OUTPUT':'/home/fidel/codeberg/urban-heat-island/data/01_01_2021_06_00_wsp_utm.shp'})
                                        
processing.run("gdal:gridinversedistance", {'INPUT':'/home/fidel/codeberg/urban-heat-island/data/01_01_2021_06_00_rh_utm.shp',
                                            'Z_FIELD':'value_RH',
                                            'POWER':2.2,
                                            'SMOOTHING':0,
                                            'RADIUS_1':0,
                                            'RADIUS_2':0,
                                            'ANGLE':0,
                                            'MAX_POINTS':0,
                                            'MIN_POINTS':0,
                                            'NODATA':0,
                                            'OPTIONS':'',
                                            'EXTRA':'',
                                            'DATA_TYPE':5,
                                            'OUTPUT':'/home/fidel/codeberg/urban-heat-island/data/01_01_2021_06_00_RH_pre.tif'})
                                            
processing.run("gdal:gridinversedistance", {'INPUT':'/home/fidel/codeberg/urban-heat-island/data/01_01_2021_06_00_wsp_utm.shp',
                                            'Z_FIELD':'value_WSP',
                                            'POWER':2.2,
                                            'SMOOTHING':0,
                                            'RADIUS_1':0,
                                            'RADIUS_2':0,
                                            'ANGLE':0,
                                            'MAX_POINTS':0,
                                            'MIN_POINTS':0,
                                            'NODATA':0,
                                            'OPTIONS':'',
                                            'EXTRA':'',
                                            'DATA_TYPE':5,
                                            'OUTPUT':'/home/fidel/codeberg/urban-heat-island/data/01_01_2021_06_00_WSP_pre.tif'})    
#BASE_DIR = Path(__file__).resolve().parent
#data_dir = os.path.join(BASE_DIR,"data")
data_dir = "/home/fidel/codeberg/urban-heat-island/data"
os.chdir(data_dir)

variables = ["TMP"]

for variable in variables:
    params = {'POINTS':'01_01_2021_06_00_utm.shp',
                'FIELD':'value_'+variable,
                'TARGET_USER_XMIN TARGET_USER_XMAX TARGET_USER_YMIN TARGET_USER_YMAX':None,
                'TARGET_USER_SIZE':100,
                'PREDICTION':'01_01_2021_06_00_'+variable,
                'VARIANCE':'01_01_2021_06_00_var_'+variable,
                'TQUALITY':1,
                'VAR_MAXDIST':0,
                'VAR_NCLASSES':100,
                'VAR_NSKIP':1,
                'VAR_MODEL':'a + b * x',
                'LOG':False,
                'BLOCK':False,
                'DBLOCK':100,
                'CV_METHOD':0,
                'CV_SUMMARY':'TEMPORARY_OUTPUT',
                'CV_RESIDUALS':'TEMPORARY_OUTPUT',
                'CV_SAMPLES':10,
                'SEARCH_RANGE':1,
                'SEARCH_RADIUS':1000,
                'SEARCH_POINTS_ALL':1,
                'SEARCH_POINTS_MIN':16,
                'SEARCH_POINTS_MAX':20}
                    
    processing.run("sagang:ordinarykriging", params)
    
for variable in variables:
    processing.run("gdal:translate", {'INPUT': '01_01_2021_06_00_'+variable+'.sdat',
                    'TARGET_CRS':None,
                    'NODATA':None,
                    'COPY_SUBDATASETS':False,
                    'OPTIONS':'','EXTRA':'','DATA_TYPE':0,
                    'OUTPUT':'01_01_2021_06_00_'+variable+'_pre.tif'})


for p in Path(data_dir).glob('*_var_*'):
    p.unlink()
    

#raster calculator para clipear



entries = []
wsp_pre_layer = QgsRasterLayer('/home/fidel/codeberg/urban-heat-island/data/01_01_2021_06_00_WSP_pre.tif', 'WSP_pre')
wsp_pre = QgsRasterCalculatorEntry()
wsp_pre.ref = 'wsp_pre@1'
wsp_pre.raster = wsp_pre_layer
wsp_pre.bandNumber = 1
entries.append( wsp_pre )

rh_layer = QgsRasterLayer('/home/fidel/codeberg/urban-heat-island/data/01_01_2021_06_00_RH_pre.tif', 'RH')
rh = QgsRasterCalculatorEntry()
rh.ref = 'rh@1'
rh.raster = rh_layer
rh.bandNumber = 1
entries.append( rh )


calc = QgsRasterCalculator('(rh@1 * 0) + wsp_pre@1', 
                           '/home/fidel/codeberg/urban-heat-island/data/01_01_2021_06_00_WSP.tif', 'GTiff', rh_layer.extent(), rh_layer.width(), rh_layer.height(), entries)


calc.processCalculation()

entries = []
tmp_layer = QgsRasterLayer('/home/fidel/codeberg/urban-heat-island/data/01_01_2021_06_00_TMP_pre.tif', 'TMP_pre')
tmp = QgsRasterCalculatorEntry()
tmp.ref = 'tmp@1'
tmp.raster = tmp_layer
tmp.bandNumber = 1
entries.append( tmp )

rh_layer = QgsRasterLayer('/home/fidel/codeberg/urban-heat-island/data/01_01_2021_06_00_RH_pre.tif', 'RH')
rh = QgsRasterCalculatorEntry()
rh.ref = 'rh@1'
rh.raster = rh_layer
rh.bandNumber = 1
entries.append( rh )


calc = QgsRasterCalculator('(rh@1 * 0) + tmp@1', 
                           '/home/fidel/codeberg/urban-heat-island/data/01_01_2021_06_00_TMP.tif', 'GTiff', rh_layer.extent(), rh_layer.width(), rh_layer.height(), entries)


calc.processCalculation()


entries = []


rh_layer = QgsRasterLayer('/home/fidel/codeberg/urban-heat-island/data/01_01_2021_06_00_RH_pre.tif', 'RH')
rh = QgsRasterCalculatorEntry()
rh.ref = 'rh@1'
rh.raster = rh_layer
rh.bandNumber = 1
entries.append( rh )


calc = QgsRasterCalculator('(rh@1 * 0) + rh@1', 
                           '/home/fidel/codeberg/urban-heat-island/data/01_01_2021_06_00_RH.tif', 'GTiff', rh_layer.extent(), rh_layer.width(), rh_layer.height(), entries)


calc.processCalculation()


