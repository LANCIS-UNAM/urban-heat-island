# A vegetation model of Urban Heat Island

We have developed a spacialy explicit model for the computation of
urban heat islands.

It is based on [this model](https://doi.org/10.2134/jeq2015.01.0056)
which considers radiation and other atmosferic factors along with
vegetation to compute heat flux in places with previous temperature,
humidity and wind data.

We developed Python APIs for our [heat flux model](uhi/__init__.py)
and [vegetation model](uhi/planta.py). Then used [this
notebook](single_day_data_trial.ipynb) to explore different ranges of
inputs.

We then use historic data gathered by weather stations. In order to
cover areas we are must interpolate from different points. We use a
grid inverse distance algorithm provided by QGIS.

Together with our vegetation model we are able to simulate different
system trajectories, depending on season, vegetation coverage and
vegetation type.

Our model can be useful mixed with other software such as urban growth
models and climate prediction models for exploratory simulation or for
use as a boundary object, to engage authorities and other parties in
the planning of reforestation efforts, legislation and other similar
activities.



## Data flow

We have developed our model based on specific databases for Mexico
City.

Along with the model we put together an Extract-Transform-Load
pipeline which handles the necessary formatting. Raw data is available
in exchange formats, such as comma separated values, which must be
converted into the SHP format for explicit spatial data.

Within QGIS we interpolate point data to cover areas between stations,
thus creating a rasters for temperature, relative humidity and wind
speed.

These rasters are inputs to a script which wraps our heat flux model
along with our vegetation model and computes output values for each
cell.

![data flow](dataflow.png)


### Weather data for Mexico City region

We downloaded these data sets: <http://www.aire.cdmx.gob.mx/default.php?opc=%27aKBhnmI=%27&opcion=Zw==>

Take a look at the makefile in this repo, which we include for easy reproducibility.

```
cd data
make
```

