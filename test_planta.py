import uhi
from uhi import planta as plnt
from uhi import secundarias as sec

import pytest

p = plnt.Planta()


hora = 6.0
temp_abs = 290.6
temp_aire = 17.6
hr = 89.0
DPV = sec.DPV(sec.es(temp_aire), sec.e(sec.es(temp_aire), hr))

velocidad_viento = 0.85
Cp = sec.calor_especifico_aire()

estacion = uhi.Estacion(lon=99.11, lat=19.429)


def test_g_QN():
    assert p.g(QN=estacion.QN(hora, temp_abs)) == pytest.approx(1.12, rel=1e-2)

def test_g_temp_aire():
    assert p.g(temp_aire=temp_aire) == pytest.approx(0.7, rel=1e-1)

def test_g_DPV():
    assert p.g(DPV=DPV) == pytest.approx(1.29, rel=1e-2)
    
def test_gS():
    assert p.gS(estacion.QN(hora, temp_abs), temp_aire, DPV) == pytest.approx(1.20028905894053)

def test_rD():
    assert p.rD(estacion.QN(hora, temp_abs), temp_aire, DPV) == pytest.approx(36.2231585347804)
            
def test_rA():
    assert p.rA(velocidad_viento) == pytest.approx(21.875714921037)
                
def test_s():
    assert p.s(temp_aire, estacion.QN(hora, temp_abs)) == pytest.approx(-224.083020111482)
                    
def test_rACP():
    assert p.rACP(Cp, temp_aire, DPV, velocidad_viento) == pytest.approx(12.321638789357)
                        
def test_QE():
    assert p.QE(Cp, temp_aire, estacion.QN(hora, temp_abs), DPV, velocidad_viento) == pytest.approx(-35.7841937995982)
                            
