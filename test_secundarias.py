from uhi import secundarias as sec
import pytest

temp_aire = 17.7092181761473
hr = 89.4

def test_constante_psicometrica():
    assert sec.constante_psicometrica(temp_aire) == pytest.approx(0.065006255309057)

def test_S():
    assert sec.S(temp_aire) == pytest.approx(2.26160936095539)

def test_calor_especifico_aire():
    assert sec.calor_especifico_aire() == pytest.approx(1011.67)

def test_rho_A():
    assert sec.rho_A(temp_aire) == pytest.approx(1.20292483093688)

def test_lambda_V():
    assert sec.lambda_V(temp_aire) == pytest.approx(2.45849787637725)

def test_es():
    assert sec.es(temp_aire) == pytest.approx(2.02655320462691)

def test_e():
    assert sec.e(sec.es(temp_aire), hr) == pytest.approx(1.81173856493645)

def test_DPV():
    assert sec.DPV(sec.es(temp_aire), sec.e(sec.es(temp_aire), hr)) == pytest.approx(0.214814639690452)
