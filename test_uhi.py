from uhi import Estacion
from uhi import secundarias as sec
from uhi import planta as plnt

import pytest

estacion = Estacion(lon=-99.11, lat=19.429)

hora = 7.0
temp_abs = 290.7
temp_abs1 = temp_abs
temp_abs0 = 290.9
temp_aire = 17.7

S = sec.S(temp_aire)


hr = 89.0
DPV = sec.DPV(sec.es(temp_aire), sec.e(sec.es(temp_aire), hr))
velocidad_viento = 0.85
Cp = sec.calor_especifico_aire()

p = plnt.Planta()
QE = p.QE(Cp, temp_aire, estacion.QN(hora, temp_abs), DPV, velocidad_viento)

def test_razon_R_r():
    assert estacion.razon_R_r() == pytest.approx(1.05837538203855)

def test_declinacion():
    assert estacion.declinacion() == pytest.approx(21.6666397863361)

def test_et():
    assert estacion.et() == pytest.approx(2.63726373163344)

def test_to():
    assert estacion.to() == pytest.approx(12.09, rel=1e-2)

def test_h():
    assert estacion.h(hora) == pytest.approx(-76.3371780677849)

def test_td():
    assert estacion.td() == pytest.approx(47066.5306125278)

def test_cos_theta():
    assert estacion.cos_theta(hora) == pytest.approx(0.329830453098121)

def test_theta():
    assert estacion.theta(hora) == pytest.approx(70.7415149886279)

def test_m():
    assert estacion.m(hora) == pytest.approx(2.18046819551254)

def test_RG():
    assert estacion.RG(hora) == pytest.approx(28.5, rel=1.e-2)

def test_ILD():
    assert estacion.ILD(hora, temp_abs) == pytest.approx(-100.5, rel=1e-2)

def test_QN():
    assert estacion.QN(hora, temp_abs) == pytest.approx(-75.4, rel=1e-3)

def test_delta_QS():
    assert estacion.delta_QS(hora=hora, temp_abs0=temp_abs0, temp_abs1=temp_abs1) == pytest.approx(-44.411472938624, rel=1e-3)

def test_QUE():
    assert estacion.QUE(S, hora, temp_abs0, temp_abs1) == pytest.approx(-13.0035691232261, rel=1e-3)

def test_QHU():
    assert estacion.QHU(S, hora, temp_abs0, temp_abs1) == pytest.approx(-17.9875403487125, rel=1e-3)

def test_TD_PU():
    assert estacion.TD_PU(hora, S, temp_abs0, temp_abs1) == pytest.approx(16.903151962552, rel=1e-3)

def test_QHV():
    assert estacion.QHV(hora, temp_abs0, temp_abs1, QE) == pytest.approx(-4.39608404972087, rel=1e-1)

def test_TD_PV():
    assert estacion.TD_PV(hora, temp_abs0, temp_abs1, QE) == pytest.approx(17.5052534765974, rel=1e-3)
    
