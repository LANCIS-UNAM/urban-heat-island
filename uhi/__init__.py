from math import cos, radians, pi, sin, degrees, acos, tan, pow
from dataclasses import dataclass


@dataclass
class Estacion:

    lat: float
    lon: float

    orientacion: float = 0
    pendiente: float = 0

    dia: int = 150
    albedo: float = 0.12

    #TODO: argumento es visibilidad, de ahi deducir a y b    
    a: float = 0.793
    b: float = 0.305

    presion_atmosferica_media: float = 735

    a1: float = 0.38
    a2: float = 0.56
    a3: float = -27.4

    alfaPM: float = 2
    betaPM: float = 6
    
    def razon_R_r(self):
        return (1
                /
                (1 + (0.033
                      * cos(self.dia
                            * 2
                            * pi / 365))))**2


    def declinacion(self):
        return -23.4 * (cos(radians(360*(self.dia+10)/365)))


    def et(self):
        """
        Ecuación del tiempo
        """
        return 9.87 * sin(radians(2 * 360*(self.dia - 81)/365)) - (7.53*cos(radians(360*(self.dia-81)/365)))-(1.5*sin(radians(360*(self.dia-81)/365)))


    def to(self):
        """
        mediodia solar
        """
        return 12 + ((7 + ((((720-self.et() - 4 * self.lon) / 60) - 420)/60)))
        #=12+((7+((((720-P9-4*$O$1)/60)-420)/60)))


    def h(self, hora):
        """
        Angulo horario
        """
        return 15*(hora - self.to())


    def td(self):
        """
        duración del día
        """
        return (2*degrees(acos((-tan(radians(self.lat))*(tan(radians(self.declinacion()))))))/15) * 3600



    def cos_theta(self, hora):
        """
        Coseno del ángulo entre la superficie y el rayo de sol
        """
        return ((((sin(radians(self.lat))*(cos(radians(self.h(hora))))*((-cos(radians(self.orientacion))*(sin(radians(self.pendiente))))))-((sin(radians(self.h(hora)))*((sin(radians(self.orientacion))*(sin(radians(self.pendiente)))))))+((((cos(radians(self.lat))*(cos(radians(self.h(hora)))))*(cos(radians(self.pendiente))))))))*(cos(radians(self.declinacion()))))+((cos(radians(self.lat))*((cos(radians(self.orientacion))))*(sin(radians(self.pendiente))))+((sin(radians(self.lat))*(cos(radians(self.pendiente))))))*(sin(radians(self.declinacion())))


    def theta(self, hora):
        """
        Angulo entre la superficie y el sol
        """
        return degrees(acos(self.cos_theta(hora)))


    def m(self, hora):
        """
        Camino óptico de la masa de aire
        """
        return (1/(self.cos_theta(hora) + (0.15 * (pow  ((93.885 - self.theta(hora)),-1.253)))))*(self.presion_atmosferica_media/ 1013)


    def RG(self, hora):
        """
        Radiacion solar global con m
        """
        return self.razon_R_r()  * 1370 * self.cos_theta(hora) * self.a * pow(self.b, self.m(hora))

    def ILD(self, hora, temp_abs):
        """
        Radiación de onda larga
        """
        return (0.0000000567*(pow(temp_abs - 20,4)))-(0.0000000567*pow(temp_abs,4))



    def QN(self, hora, temp_abs):
        """
        Radiación neta (calculada con m)
        """
        return (self.RG(hora)
                - self.albedo * self.RG(hora)) + self.ILD(hora, temp_abs)


    def delta_QS(self, hora, temp_abs0, temp_abs1):
        """
        Energía almacenda
        """
        return self.a1 * self.QN(hora, temp_abs1) + self.a2 * (self.QN(hora, temp_abs1) - self.QN(hora - 0.5, temp_abs0)) + self.a3
#              $AA$2    * Z10                      + $AA$3   * (Z10                      - Z9                            )+$AA$4


    def QUE(self, S, hora, temp_abs0, temp_abs1):
        """
        Flujo de calor latente urbano
        """
        return ((self.alfaPM) / (1 + S)) * (self.QN(hora, temp_abs1) - self.delta_QS(hora, temp_abs0, temp_abs1)) + self.betaPM


    def QHU(self, S, hora, temp_abs0, temp_abs1):
        """
        Flujo de calor sensible urbano
        """
        return (((1-self.alfaPM + S) / (1 + S)) * (self.QN(hora, temp_abs1) - self.delta_QS(hora, temp_abs0, temp_abs1)) - self.betaPM)


    def TD_PU(self, hora, S, temp_abs0, temp_abs1):
        """
        TD/PU
        Temperatura diagnóstico/pronóstico urbano
        """
        return 0.0443 * self.QHU(S, hora, temp_abs0, temp_abs1) + 17.7


    def QHV(self, hora, temp_abs0, temp_abs1, QE):
        """
        Flujo de calor sensible vegetal
        """
        return self.QN(hora, temp_abs1) - (self.delta_QS(hora, temp_abs0, temp_abs1) + QE)

    
    def TD_PV(self, hora, temp_abs0, temp_abs1, QE):
        """
        TD/PV
        Temperatura diagnóstico/pronóstico vegetal
        """
        return 0.0443 * self.QHV(hora, temp_abs0, temp_abs1, QE) + 17.7


    
