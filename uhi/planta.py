from uhi.secundarias import S, rho_A, lambda_V, constante_psicometrica
from dataclasses import dataclass
from math import log
from operator import xor


@dataclass
class Planta:

    A:     float = 1.01
    B:     float = 10
    alfa:  float = 1.85
    beta:  float = -0.252
    gSMAX: float = 1.25
    a:     float = -9.799
    b:     float = 0.964
    c:     float = -0.021
    d:     float = 2    
    ZW:    float = 5
    Z0:    float = 1
    K:     float = 0.4
    IAF:   float = 2.3

    def g(self, QN=None, temp_aire=None, DPV=None):
        # provide one of QN, temp_aire or DPV
        assert xor(xor(QN is not None, temp_aire is not None), DPV is not None)
        
        if QN:
            # ĝ (QN)
            return (QN * self.A) / (QN + self.B)
        elif temp_aire:
            # ĝ (temp_aire)
            return self.a + (temp_aire * self.b) + ((temp_aire**2) * self.c)
        elif DPV:
            # ĝ (DPV)
            return (DPV * 10 * self.beta) + self.alfa


    def gS(self, QN, temp_aire, DPV):
        """
        Conductancia Estomática
        """
        return self.gSMAX * self.g(QN=QN) * self.g(temp_aire=temp_aire) * self.g(DPV=DPV)


    def rD(self, QN, temp_aire, DPV):
        """
        Resistencia del Dosel
        """
        return (1/(self.IAF * self.gS(QN, temp_aire, DPV))) * 100


    def rA(self, velocidad_viento):
        """
        Resistencia aerodinámica
        """
        return (1
                /
                (self.K**2 * velocidad_viento)) * log((self.ZW - self.d) / self.Z0)  * log((self.ZW - self.d) / (0.2 * self.Z0))


    def s(self, temp_aire, QN):
        """
        """
        return S(temp_aire) * QN


    def rACP(self, Cp, temp_aire, DPV, velocidad_viento):
        """
        rACP(DPV)/rA
        @param Cp calor específico del aire
        """
        return (Cp * rho_A(temp_aire) * DPV) / self.rA(velocidad_viento)


    def QE(self, Cp, temp_aire, QN, DPV, velocidad_viento):
        """
        Flujo de calor latente
        """
        return ((self.s(temp_aire, QN) + self.rACP(Cp, temp_aire, DPV, velocidad_viento))
                /
                ( S(temp_aire) + constante_psicometrica(temp_aire) * ( 1 + (self.rD(QN, temp_aire, DPV)/self.rA(velocidad_viento))))
                /
                lambda_V(temp_aire))
    
