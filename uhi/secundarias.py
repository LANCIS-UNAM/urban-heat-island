import math

# Variables derivadas secundarias

def constante_psicometrica(temp_aire):
    """
    g (kPa/K)
    """
    return 0.000006 * temp_aire + 0.0649


def S(temp_aire):
    """
    Pendiente de la presión de vapor de saturación
    """
    return ((4098 * (es(temp_aire) * temp_aire))
            / ((temp_aire + 237.3)**2))



# TODO
def calor_especifico_aire():
    """
    Calor específico del aire
    Cp (J /kg K)
    """
    return 1011.67



def rho_A(temp_aire):
    """
    Densidad del aire
    rA (kg/m3)
    """
    return -0.0049 * temp_aire + 1.2897



def lambda_V(temp_aire):
    """
    Calor latente de vaporización
    lv (kJ/g)
    """
    return -0.0024 * temp_aire + 2.501


def es(temp_aire):
    """
    Presión de vapor de saturación
    es  (kPa)
    """
    return 0.6108 * math.exp( (17.27 * temp_aire)
                              / (temp_aire + 237.3))


def e(es, hr):
    """
    Presión de vapor actual
    e   (kPa)
    @param hr humedad relativa
    """
    return (es * hr) / 100



def DPV(es, e):
    """
    kPa
    """
    return es - e
